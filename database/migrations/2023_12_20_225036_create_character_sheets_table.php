<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCharacterSheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('character_sheets', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('jobs_id');
            $table->string('name', 255);
            $table->string('lastname', 255);
            $table->integer('year');
            $table->string('sexe');
            $table->integer('size');
            $table->integer('pv');
            $table->integer('moral');
            $table->integer('mana');
            $table->integer('strength');
            $table->integer('intelligence');
            $table->integer('charism');
            $table->integer('agility');
            $table->integer('sorcery');
            $table->integer('status_points');
            $table->uuid('races_id');
            $table->uuid('classes_id');
            $table->uuid('subclasses_id');
            $table->foreign('races_id')->references('id')->on('races');
            $table->foreign('classes_id')->references('id')->on('classes');
            $table->foreign('subclasses_id')->references('id')->on('subclasses');
            $table->foreign('jobs_id')->references('id')->on('jobs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('character_sheets');
    }
}
